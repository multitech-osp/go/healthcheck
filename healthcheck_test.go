package healthcheck

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/multitech-osp/go/log"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	log.Init()
	m.Run()
}

func TestInit(t *testing.T) {

	go Init()

	req := httptest.NewRequest(http.MethodGet, "/health", nil)
	w := httptest.NewRecorder()
	HealthRoute(w, req)
	res := w.Result()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Print(err)
	}

	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Contains(t, string(data), string(`{"status":"UP"}`))
}
