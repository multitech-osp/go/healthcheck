package healthcheck

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/multitech-osp/go/log"
)

var DEFAULT_HEALTH_CHECK_PORT = "3000"

type Health struct {
	Status string `json:"status"`
}

func HealthRoute(w http.ResponseWriter, r *http.Request) {
	var health Health
	health.Status = "UP"
	json.NewEncoder(w).Encode(health)
}

func Init() {
	healthCheckPort := os.Getenv("HEALTHCHECK_PORT")
	if healthCheckPort == "" {
		healthCheckPort = DEFAULT_HEALTH_CHECK_PORT
	}

	http.HandleFunc("/health", HealthRoute)
	log.Info(fmt.Sprintf("Health check was start on port :%s", healthCheckPort))
	err := http.ListenAndServe(fmt.Sprintf(":%s", healthCheckPort), nil)
	log.Fatal(err.Error())
}
